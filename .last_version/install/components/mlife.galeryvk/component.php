 <?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
	
	// ���� �����������
	$this->SetResultCacheKeys(array(
		"IST",
		"ID_IST",
		"ID_GAL",
		"KOL_PHOTO",
		"READMORE",
	));

	if ($this->StartResultCache())
	{
		switch($arParams['IST']) {
			case 'U':
			$usertype  = 'oid';
			break;
			case 'G':
			$usertype  = 'gid';
			break;
			default:
			$usertype  = 'oid';
			break;
		}
		$photo_sizes = intval($arParams['PHOTO_SIZES']);
		
		$url = 'https://api.vk.com/method/photos.get?v=5.100&count=1000&owner_id='.($arParams['IST']=='G' ? '-' : '').$arParams['ID_IST'].'&album_id='.$arParams['ID_GAL'].'&extended=0&feed_type=photo&photo_sizes='.$photo_sizes.'&access_token='.$arParams['ID_KEY'];
		$httpClient = new \Bitrix\Main\Web\HttpClient();
		$httpClient->disableSslVerification();
		$arrfotos = $httpClient->get($url);

		$getarr = false;
		if($arrfotos){
		$getarr = json_decode($arrfotos);
		$error_msg = $getarr->error->error_msg;
		}else{
		$error_msg = '��� ���������� � ��������';
		}
		
		if($arParams['KOL_PHOTO']==0) $arParams['KOL_PHOTO']=10000;
		
		if($getarr && count($getarr->response->items)>0) {
			$i=0;$count = $arParams['KOL_PHOTO'];
			foreach($getarr->response->items as $key=>$listfoto) {
				if($i<$count) {
				$fotosrc = '';
				if(is_array($listfoto->sizes)){
					$sizebig = 0;
					$fotosrc_prew = false;
					foreach($listfoto->sizes as $size) {
						if($sizebig<intval($size->width)) {
							$sizebig = $size->width;
							$fotosrc = $size->url;
						}
						if($size->width=='130') $fotosrc_prew = $size->url;
					}
				}
					if($fotosrc){
						if ($fotosrc_prew) {
							$arResult['photo'][$key]['src'] = $fotosrc_prew;
						}else{
							$arResult['photo'][$key]['src'] = $listfoto->url;
						}
						$arResult['photo'][$key]['created'] = $listfoto->date;
						$arResult['photo'][$key]['text'] = $listfoto->text;
						$arResult['photo'][$key]['src_big'] = $fotosrc;
					}
				
				}else{
				break;
				}
				$i++;
			}
			unset($getarr);
			$arResult['error_msg']=$error_msg;
		}
		
		if($arParams['READMORE']==1) {
		$creat = '';
		if($arParams['IST']=='G') $creat = '-';
		$arResult['show_readmore_href'] = 'http://vk.com/album'.$creat.$arParams['ID_IST'].'_'.$arParams['ID_GAL'];
		}
		
		$this->IncludeComponentTemplate();
	}
?>